### TaskPOC
This app has two screens.
1. The task list screen that fetches all the tasks from the database if we have any.
2. The Task detail or create task screen that lets users create a task

For now, users can add title and description that would be added and displayed on the task list screen.
Unit test has been written for the `TaskListViewModel` class

#### Architecture
- This app uses MVVM as the architecture with koin for injections.
- Room is used to store tasks created.
- LiveData and Flow is used to navigate between screens and also to emit view state/view events
- EventPipeline is used to propagate changes across several screens

#### Whats pending:
- Ability to view an already created task in the Task Detail Screen
- Date Time picker has not been implemented
- updating an already created task

#### Whats can be improved
- More classes can be unit tested like for instance the repositories/mappers
- We could use a paginator (paging library) to load only a few items from the database and then load more as we scroll
- Validations can be added to prevent empty fields from being saved into the database
