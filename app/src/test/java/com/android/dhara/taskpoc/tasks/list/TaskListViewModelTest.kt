package com.android.dhara.taskpoc.tasks.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.android.dhara.taskpoc.logger.Logger
import com.android.dhara.taskpoc.repository.TaskRepository
import com.android.dhara.taskpoc.repository.pipelines.EventPipelines
import com.android.dhara.taskpoc.repository.pipelines.events.TasksAction
import com.android.dhara.taskpoc.tasks.list.data.TaskListSingleViewState
import com.android.dhara.taskpoc.tasks.list.data.TaskListViewEvent
import com.android.dhara.taskpoc.tasks.list.data.TaskListViewState
import com.android.dhara.taskpoc.test.TestCoroutineRule
import com.flextrade.kfixture.KFixture
import kotlin.test.assertTrue
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

class TaskListViewModelTest {
    @Suppress("unused")
    @get:Rule
    val coroutineTestRule = TestCoroutineRule()

    @Suppress("unused")
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val mockTaskRepository = mock<TaskRepository>()
    private val mockLogger = mock<Logger>()

    private val eventPipelines = EventPipelines()
    private val fixture = KFixture()

    private lateinit var viewModel: TaskListViewModel

    @Before
    fun setUp() {
        runBlockingTest {
            whenever(mockTaskRepository.getAllTasks()).thenReturn(emptyList())
        }
    }

    @Test
    fun `When viewModel is loaded Then Loader is shown`() {
        runBlockingTest {
            coroutineTestRule.testCoroutineDispatcher.pauseDispatcher()

            viewModel = createViewModel()
            assertTrue(viewModel.viewState.first() is TaskListViewState.Loading)
        }
    }

    @Test
    fun `Given ViewModel is initialized When tasks are fetched Then the result is propagated`() {
        runBlockingTest {
            whenever(mockTaskRepository.getAllTasks()).thenReturn(listOf(fixture()))

            viewModel = createViewModel()
            verify(mockTaskRepository).getAllTasks()

            assertTrue(viewModel.viewState.value is TaskListViewState.Success)
        }
    }

    @Test
    fun `Given ViewModel is initialized When tasks are fetched and there is an error Then an error is propagated`() {
        runBlockingTest {
            whenever(mockTaskRepository.getAllTasks()).thenThrow(RuntimeException("Error"))

            viewModel = createViewModel()
            verify(mockTaskRepository).getAllTasks()

            assertTrue(viewModel.viewState.first() is TaskListViewState.Error)
        }
    }

    @Test
    fun `When OnCreateNewTaskClicked is propagated Then user is navigated to create task screen`() {
        viewModel = createViewModel()

        viewModel.onViewEvent(TaskListViewEvent.OnCreateNewTaskClicked)

        runBlockingTest {
            assertTrue(viewModel.eventFlow.first() is TaskListSingleViewState.LaunchCreateTaskScreen)
        }
    }

    @Test
    fun `When TaskCreatedAction event pipeline is filtered Then screen is refreshed`() {
        viewModel = createViewModel()

        runBlocking { eventPipelines.tasksActionPipeline.emit(TasksAction.TaskCreatedAction) }

        runBlockingTest {
            verify(mockTaskRepository, times(2)).getAllTasks()
        }
    }

    private fun createViewModel() = TaskListViewModel(
        taskRepository = mockTaskRepository,
        eventPipelines = eventPipelines,
        logger = mockLogger
    )
}
