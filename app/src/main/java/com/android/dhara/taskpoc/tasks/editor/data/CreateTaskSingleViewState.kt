package com.android.dhara.taskpoc.tasks.editor.data

sealed class CreateTaskSingleViewState {
    object ErrorSavingTask : CreateTaskSingleViewState()
    object CloseScreen : CreateTaskSingleViewState()
    object Success : CreateTaskSingleViewState()
}
