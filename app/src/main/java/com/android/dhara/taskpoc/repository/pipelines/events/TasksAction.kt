package com.android.dhara.taskpoc.repository.pipelines.events

sealed class TasksAction {
    object TaskCreatedAction : TasksAction()
}
