package com.android.dhara.taskpoc.logger

import com.android.dhara.taskpoc.BuildConfig

class TaskPocLogger : Logger {
    override fun log(error: Throwable) {
        if (BuildConfig.DEBUG) {
            error.printStackTrace()
        }
    }
}
