package com.android.dhara.taskpoc.ui.decorations

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class MarginsDecoration(
    private val marginSides: Int,
    private val marginTop: Int,
    private val marginBetween: Int,
    @RecyclerView.Orientation
    private val orientation: Int = RecyclerView.VERTICAL
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        if (orientation == RecyclerView.VERTICAL) {
            getVerticalOffsets(outRect, view, parent)
        } else {
            getHorizontalOffsets(outRect, view, parent)
        }
    }

    private fun getVerticalOffsets(outRect: Rect, view: View, parent: RecyclerView) {
        // Add margin to both sides
        outRect.right = marginSides
        outRect.left = marginSides

        // Half of `marginBetween` for all items at the bottom
        outRect.bottom = Math.floor(marginBetween.toDouble() / 2).toInt()

        // Half of `marginBetween` for all items except the first at the top
        // Top margin only for item at the top
        val position = parent.getChildLayoutPosition(view)
        if (position == 0) {
            outRect.top = marginTop
        } else if (position > 0) {
            outRect.top = Math.ceil(marginBetween.toDouble() / 2).toInt()
        }
    }

    private fun getHorizontalOffsets(outRect: Rect, view: View, parent: RecyclerView) {
        if (parent.layoutDirection == View.LAYOUT_DIRECTION_RTL) {
            getHorizontalOffsetsRtl(outRect, view, parent)
        } else {
            getHorizontalOffsetsLtr(outRect, view, parent)
        }
    }

    private fun getHorizontalOffsetsRtl(outRect: Rect, view: View, parent: RecyclerView) {
        // Add margin to Top
        outRect.top = marginTop
        // Half of `marginBetween` for all items except the first at the right and last at the left
        val position = parent.getChildLayoutPosition(view)
        when {
            parent.adapter?.itemCount == 1 -> {
                outRect.left = marginSides
                outRect.right = marginSides
            }
            position == 0 -> {
                outRect.right = marginSides
                outRect.left = Math.floor(marginBetween.toDouble() / 2).toInt()
            }
            position + 1 == parent.adapter?.itemCount -> {
                outRect.left = marginSides
                outRect.right = Math.floor(marginBetween.toDouble() / 2).toInt()
            }
            position > 0 -> {
                outRect.left = Math.ceil(marginBetween.toDouble() / 2).toInt()
                outRect.right = Math.ceil(marginBetween.toDouble() / 2).toInt()
            }
        }
    }

    private fun getHorizontalOffsetsLtr(outRect: Rect, view: View, parent: RecyclerView) {
        // Add margin to Top
        outRect.top = marginTop
        // Half of `marginBetween` for all items except the first at the left and last at the right
        val position = parent.getChildLayoutPosition(view)
        when {
            parent.adapter?.itemCount == 1 -> {
                outRect.left = marginSides
                outRect.right = marginSides
            }
            position == 0 -> {
                outRect.left = marginSides
                outRect.right = Math.floor(marginBetween.toDouble() / 2).toInt()
            }
            position + 1 == parent.adapter?.itemCount -> {
                outRect.right = marginSides
                outRect.left = Math.floor(marginBetween.toDouble() / 2).toInt()
            }
            position > 0 -> {
                outRect.left = Math.ceil(marginBetween.toDouble() / 2).toInt()
                outRect.right = Math.ceil(marginBetween.toDouble() / 2).toInt()
            }
        }
    }
}
