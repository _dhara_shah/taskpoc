package com.android.dhara.taskpoc.tasks.list

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.android.dhara.taskpoc.entity.Task
import com.android.dhara.taskpoc.ui.adapter.ViewHolderFactory

class TaskListAdapter(
    private val viewHolderFactory: ViewHolderFactory<TaskListViewHolder>
) : ListAdapter<Task, TaskListViewHolder>(DIFF_CALLBACK) {

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskListViewHolder {
        return viewHolderFactory(parent, viewType)
    }

    override fun onBindViewHolder(holder: TaskListViewHolder, position: Int) {
        getItem(position)?.let { task ->
            holder.bindItem(task)
        }
        holder.bindItem(getItem(position))
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Task>() {
            override fun areItemsTheSame(oldItem: Task, newItem: Task) = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Task, newItem: Task) = oldItem == newItem
        }
    }
}
