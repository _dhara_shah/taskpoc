package com.android.dhara.taskpoc.logger

interface Logger {
    fun log(error: Throwable)
}
