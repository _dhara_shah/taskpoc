package com.android.dhara.taskpoc.repository.room

import android.content.Context
import androidx.room.Room
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DatabaseHelper(
    context: Context,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {
    internal val database =
        Room.databaseBuilder(context, TaskPocDatabase::class.java, "tasks.db").build()

    suspend fun tearDownDatabase() {
        withContext(dispatcher) {
            database.clearAllTables()
        }
    }
}



