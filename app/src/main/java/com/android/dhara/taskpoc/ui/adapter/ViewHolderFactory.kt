package com.android.dhara.taskpoc.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

fun interface ViewHolderFactory<T : RecyclerView.ViewHolder> : (ViewGroup, Int) -> T
