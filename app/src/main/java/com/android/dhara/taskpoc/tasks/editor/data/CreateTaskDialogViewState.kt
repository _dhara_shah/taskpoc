package com.android.dhara.taskpoc.tasks.editor.data

sealed class CreateTaskDialogViewState {
    object ShowSuccess : CreateTaskDialogViewState()
    object ShowConfirmCancelDialog : CreateTaskDialogViewState()
}
