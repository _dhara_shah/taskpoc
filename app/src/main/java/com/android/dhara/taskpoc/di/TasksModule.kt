package com.android.dhara.taskpoc.di

import android.view.ViewGroup
import com.android.dhara.taskpoc.tasks.list.TaskListViewModel
import com.android.dhara.taskpoc.tasks.editor.CreateTaskViewModel
import com.android.dhara.taskpoc.tasks.list.TaskListAdapter
import com.android.dhara.taskpoc.tasks.list.TaskListViewHolder
import com.android.dhara.taskpoc.ui.adapter.ViewHolderFactory
import org.koin.dsl.module

val tasksModule = module {
    factory<TaskListViewModel> {
        TaskListViewModel(
            taskRepository = get(),
            eventPipelines = get(),
            logger = get()
        )
    }

    factory { parameters ->
        CreateTaskViewModel(
            state = parameters.get(),
            taskRepository = get(),
            logger = get(),
            eventPipelines = get()
        )
    }

    factory<TaskListAdapter> {
        TaskListAdapter(
            viewHolderFactory = ViewHolderFactory { parent: ViewGroup, _ ->
                TaskListViewHolder.create(parent)
            }
        )
    }
}
