package com.android.dhara.taskpoc.repository

import com.android.dhara.taskpoc.entity.Task
import com.android.dhara.taskpoc.logger.Logger
import com.android.dhara.taskpoc.repository.mappers.TaskMapper
import com.android.dhara.taskpoc.repository.room.TaskDao

class TaskRepository(val taskMapper: TaskMapper, val taskDao: TaskDao, val logger: Logger) {
    suspend fun insertTask(task: Task) {
        taskDao.insert(taskMapper.toDbEntity(task))
    }

    suspend fun getAllTasks() = kotlin.runCatching {
        taskDao.getAll().map { dbEntity -> taskMapper.asEntity(dbEntity) }
    }.getOrElse {
        logger.log(it)
        emptyList()
    }
}
