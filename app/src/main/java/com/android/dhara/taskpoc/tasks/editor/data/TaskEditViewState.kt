package com.android.dhara.taskpoc.tasks.editor.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import org.joda.time.DateTime

@Parcelize
data class TaskEditViewState(
    val title: String? = null,
    val description: String? = null,
    val dateTime: DateTime? = null
) : Parcelable
