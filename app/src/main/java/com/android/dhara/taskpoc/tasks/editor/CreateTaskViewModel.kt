package com.android.dhara.taskpoc.tasks.editor

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.dhara.taskpoc.entity.Task
import com.android.dhara.taskpoc.logger.Logger
import com.android.dhara.taskpoc.repository.TaskRepository
import com.android.dhara.taskpoc.repository.pipelines.EventPipelines
import com.android.dhara.taskpoc.repository.pipelines.events.TasksAction
import com.android.dhara.taskpoc.tasks.editor.data.CreateTaskDialogViewState
import com.android.dhara.taskpoc.tasks.editor.data.CreateTaskSingleViewState
import com.android.dhara.taskpoc.tasks.editor.data.CreateTaskViewEvent
import com.android.dhara.taskpoc.tasks.editor.data.TaskEditViewState
import com.android.dhara.taskpoc.tasks.editor.data.TextChangedViewEvent
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import org.joda.time.DateTime

class CreateTaskViewModel(
    private val state: SavedStateHandle,
    val taskRepository: TaskRepository,
    val logger: Logger,
    val eventPipelines: EventPipelines
) : ViewModel() {

    private fun setSavedState(taskEditViewState: TaskEditViewState) =
        state.set(TASK_SAVED_STATE_KEY, taskEditViewState)

    private fun getSavedState(): TaskEditViewState? = state.get(TASK_SAVED_STATE_KEY)

    private val _eventFlow = Channel<CreateTaskSingleViewState>(Channel.BUFFERED)
    val eventFlow = _eventFlow.receiveAsFlow()

    private val _dialogViewState = MutableLiveData<CreateTaskDialogViewState>()
    val dialogViewState = _dialogViewState

    init {
        setSavedState(
            TaskEditViewState()
        )
    }

    fun onViewEvent(viewEvent: CreateTaskViewEvent) {
        when (viewEvent) {
            CreateTaskViewEvent.SaveButtonClicked -> {
                getSavedState()?.let { viewState ->
                    viewModelScope.launch {
                        kotlin.runCatching {
                            val task = Task(
                                title = viewState.title.orEmpty(),
                                description = viewState.description.orEmpty(),
                                taskDate = viewState.dateTime ?: DateTime.now()
                            )
                            taskRepository.insertTask(task)
                        }.onSuccess {
                            _dialogViewState.value = CreateTaskDialogViewState.ShowSuccess
                            eventPipelines.tasksActionPipeline.emit(TasksAction.TaskCreatedAction)
                        }.onFailure(logger::log)
                    }
                }
            }

            CreateTaskViewEvent.BackButtonPressed -> {
                _dialogViewState.value = CreateTaskDialogViewState.ShowConfirmCancelDialog
            }

            CreateTaskViewEvent.ConfirmCancelClicked -> {
                viewModelScope.launch { _eventFlow.send(CreateTaskSingleViewState.CloseScreen) }
            }

            is TextChangedViewEvent -> handleTextChangedEvent(viewEvent)
        }
    }

    private fun handleTextChangedEvent(viewEvent: TextChangedViewEvent) {
        when(viewEvent) {
            is TextChangedViewEvent.TaskDateTimeDataChanged -> TODO()
            is TextChangedViewEvent.TaskDescDataChanged -> {
                getSavedState()?.let { setSavedState(it.copy(description = viewEvent.newDescription)) }
            }
            is TextChangedViewEvent.TaskTitleDataChanged -> {
                getSavedState()?.let { setSavedState(it.copy(title = viewEvent.newTitle)) }
            }
        }
    }

    companion object {
        const val TASK_SAVED_STATE_KEY = "task_saved_state_key"
    }
}
