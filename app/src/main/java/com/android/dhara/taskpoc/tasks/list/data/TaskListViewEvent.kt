package com.android.dhara.taskpoc.tasks.list.data

sealed class TaskListViewEvent {
    object OnCreateNewTaskClicked : TaskListViewEvent()

    object RefreshList : TaskListViewEvent()
}
