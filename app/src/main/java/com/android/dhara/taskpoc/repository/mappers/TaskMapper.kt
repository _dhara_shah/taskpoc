package com.android.dhara.taskpoc.repository.mappers

import com.android.dhara.taskpoc.entity.Task
import com.android.dhara.taskpoc.repository.room.TaskEntity
import org.joda.time.DateTime

class TaskMapper {
    fun asEntity(dbEntity: TaskEntity) = with (dbEntity) {
        Task(
            id = id,
            title = taskTitle.orEmpty(),
            description = taskDescription.orEmpty(),
            taskDate = taskDate,
            createdDate = createdDate
        )
    }

    fun toDbEntity(task: Task) = with (task) {
        TaskEntity(
            taskTitle = title,
            taskDescription = description,
            taskDate = taskDate,
            createdDate = DateTime.now()
        )
    }
}
