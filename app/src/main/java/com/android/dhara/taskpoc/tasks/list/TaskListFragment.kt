package com.android.dhara.taskpoc.tasks.list

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.dhara.taskpoc.NavGraphDirections
import com.android.dhara.taskpoc.R
import com.android.dhara.taskpoc.databinding.FragmentTaskListBinding
import com.android.dhara.taskpoc.extensions.collectInFragment
import com.android.dhara.taskpoc.extensions.showSnack
import com.android.dhara.taskpoc.tasks.list.data.TaskListSingleViewState
import com.android.dhara.taskpoc.tasks.list.data.TaskListViewEvent
import com.android.dhara.taskpoc.tasks.list.data.TaskListViewState
import com.android.dhara.taskpoc.ui.decorations.MarginsDecoration
import com.android.dhara.taskpoc.viewbinding.viewBinding
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class TaskListFragment : Fragment(R.layout.fragment_task_list) {
    private val binding by viewBinding(FragmentTaskListBinding::bind) {
        tasksListLayoutContainer.tasksListRecyclerView.adapter = null
    }
    private val viewModel by inject<TaskListViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.viewState.collectInFragment(this, ::handleViewState)

        viewModel.eventFlow.collectInFragment(this, ::handleViewModelEvent)

        binding.createNewTaskFab.setOnClickListener {
            viewModel.onViewEvent(TaskListViewEvent.OnCreateNewTaskClicked)
        }

        binding.tasksListLayoutContainer.tasksListRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(
                MarginsDecoration(
                    marginSides = resources.getDimensionPixelOffset(R.dimen.spacing_regular),
                    marginBetween = resources.getDimensionPixelOffset(R.dimen.spacing_regular),
                    marginTop = resources.getDimensionPixelOffset(R.dimen.spacing_regular)
                )
            )
        }
    }

    private fun handleViewState(viewState: TaskListViewState) {
        binding.tasksListLayoutContainer.progressBar.isVisible =
            viewState is TaskListViewState.Loading

        binding.tasksListLayoutContainer.tasksListRecyclerView.isVisible =
            viewState is TaskListViewState.Success

        when (viewState) {
            is TaskListViewState.Success -> {
                val adapter = get<TaskListAdapter> {
                    parametersOf(this@TaskListFragment, viewState.result)
                }.apply { submitList(viewState.result) }

                binding.tasksListLayoutContainer.tasksListRecyclerView.adapter = adapter
            }

            is TaskListViewState.Error ->
                showSnack(
                    requireView(),
                    viewState.error.localizedMessage ?: getString(R.string.generic_error)
                )
        }
    }

    private fun handleViewModelEvent(singleViewState: TaskListSingleViewState) {
        when (singleViewState) {
            TaskListSingleViewState.LaunchCreateTaskScreen ->
                findNavController().navigate(NavGraphDirections.actionCreateTaskFragment())
        }
    }
}
