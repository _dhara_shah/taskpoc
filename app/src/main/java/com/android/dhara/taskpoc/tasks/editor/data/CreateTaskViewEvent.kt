package com.android.dhara.taskpoc.tasks.editor.data

sealed class CreateTaskViewEvent {
    object SaveButtonClicked : CreateTaskViewEvent()
    object BackButtonPressed : CreateTaskViewEvent()
    object ConfirmCancelClicked : CreateTaskViewEvent()
}

sealed class TextChangedViewEvent : CreateTaskViewEvent() {
    data class TaskTitleDataChanged(val newTitle: String) : TextChangedViewEvent()
    data class TaskDescDataChanged(val newDescription: String) : TextChangedViewEvent()
    data class TaskDateTimeDataChanged(val newDateTime: String) : TextChangedViewEvent()
}
