package com.android.dhara.taskpoc.tasks.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.dhara.taskpoc.R
import com.android.dhara.taskpoc.databinding.ListItemTasksListBinding
import com.android.dhara.taskpoc.entity.Task
import com.android.dhara.taskpoc.extensions.getDateStringFullFormat

class TaskListViewHolder(
    private val binding: ListItemTasksListBinding,
) : RecyclerView.ViewHolder(binding.root) {

    fun bindItem(task: Task) {
        binding.taskTitleTextView.text = task.title
        binding.taskDescTextView.text = task.description
        binding.taskDateTextView.text = binding.root.context.getString(
            R.string.date_created,
            task.createdDate.getDateStringFullFormat()
        )
    }

    companion object {
        fun create(parent: ViewGroup): TaskListViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ListItemTasksListBinding.inflate(inflater, parent, false)
            return TaskListViewHolder(binding)
        }
    }
}
