package com.android.dhara.taskpoc.extensions

inline fun <T> lazyUnsynchronized(noinline initializer: () -> T): Lazy<T> =
    lazy(LazyThreadSafetyMode.NONE, initializer)
