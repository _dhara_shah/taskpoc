package com.android.dhara.taskpoc.tasks.list.data

sealed class TaskListSingleViewState {
    object LaunchCreateTaskScreen : TaskListSingleViewState()
}
