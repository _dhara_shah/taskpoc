package com.android.dhara.taskpoc.repository.room

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Update
import org.joda.time.DateTime

@Entity(tableName = TaskDao.TABLE_NAME)
data class TaskEntity(
    @ColumnInfo(name = "task_title") val taskTitle: String?,
    @ColumnInfo(name = "task_description") val taskDescription: String?,
    @ColumnInfo(name = "task_date") val taskDate: DateTime,
    @ColumnInfo(name = "created_date") val createdDate: DateTime
) {
    @PrimaryKey(autoGenerate = true) var id: Long = 0L
}

@Dao
interface TaskDao {
    companion object {
        const val TABLE_NAME = "tasks"
    }

    @Query("SELECT * FROM $TABLE_NAME ORDER BY created_date DESC")
    suspend fun getAll(): List<TaskEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(taskEntity: TaskEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(taskEntity: TaskEntity)
}
