package com.android.dhara.taskpoc.repository.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [TaskEntity::class], version = 1)
@TypeConverters(Converters::class)
abstract class TaskPocDatabase : RoomDatabase() {
    abstract fun taskDao(): TaskDao
}
