package com.android.dhara.taskpoc.extensions

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

fun DateTime?.getDateStringFullFormat(): CharSequence {
    return if (this == null) {
        ""
    } else {
        DateTimeFormat.shortDateTime().print(this)
    }
}
