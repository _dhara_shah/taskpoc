package com.android.dhara.taskpoc.repository.pipelines

import com.android.dhara.taskpoc.repository.pipelines.events.TasksAction
import kotlinx.coroutines.flow.MutableSharedFlow

class EventPipelines {
    val tasksActionPipeline: MutableSharedFlow<TasksAction> = MutableSharedFlow(replay = 0)
}
