package com.android.dhara.taskpoc.extensions

import androidx.fragment.app.Fragment
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

inline fun <T> Flow<T>.collectInFragment(
    fragment: Fragment,
    crossinline action: suspend (value: T) -> Unit
) {
    fragment.lifecycleScope.launch {
        flowWithLifecycle(fragment.viewLifecycleOwner.lifecycle).collect { action(it) }
    }
}
