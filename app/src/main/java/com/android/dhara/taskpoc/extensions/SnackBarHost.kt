package com.android.dhara.taskpoc.extensions

import androidx.annotation.StringRes
import com.google.android.material.snackbar.BaseTransientBottomBar

interface SnackBarHost {

    fun showSnackBar(
        message: String,
        @BaseTransientBottomBar.Duration length: Int = BaseTransientBottomBar.LENGTH_LONG,
        snackAction: SnackAction? = null
    )

    data class SnackAction(
        @StringRes val actionButtonRes: Int,
        val capitalizeText: Boolean = false,
        val action: () -> Unit
    )
}
