package com.android.dhara.taskpoc.tasks.list.data

import com.android.dhara.taskpoc.entity.Task

sealed class TaskListViewState {
    object Loading: TaskListViewState()
    data class Success(val result: List<Task>) : TaskListViewState()
    data class Error(val error: Throwable) : TaskListViewState()
}
