package com.android.dhara.taskpoc.repository.room

import androidx.room.TypeConverter
import org.joda.time.DateTime
import org.koin.core.component.KoinComponent

class Converters : KoinComponent {
    @TypeConverter
    fun fromLongToDateTime(value: Long?): DateTime? = value?.let { DateTime(value) }

    @TypeConverter
    fun fromDateTimeToLong(date: DateTime?): Long? = date?.millis
}
