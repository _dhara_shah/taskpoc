package com.android.dhara.taskpoc.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import org.joda.time.DateTime

@Parcelize
data class Task(
    val id: Long? = null,
    val title: String = "",
    val description: String = "",
    val taskDate: DateTime,
    val createdDate: DateTime = DateTime.now()
) : Parcelable
