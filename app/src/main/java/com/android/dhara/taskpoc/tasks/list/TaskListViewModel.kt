package com.android.dhara.taskpoc.tasks.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.dhara.taskpoc.logger.Logger
import com.android.dhara.taskpoc.repository.TaskRepository
import com.android.dhara.taskpoc.repository.pipelines.EventPipelines
import com.android.dhara.taskpoc.repository.pipelines.events.TasksAction
import com.android.dhara.taskpoc.tasks.list.data.TaskListSingleViewState
import com.android.dhara.taskpoc.tasks.list.data.TaskListViewEvent
import com.android.dhara.taskpoc.tasks.list.data.TaskListViewState
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class TaskListViewModel(
    val taskRepository: TaskRepository,
    val eventPipelines: EventPipelines,
    val logger: Logger
) : ViewModel() {

    private val _eventFlow = Channel<TaskListSingleViewState>(Channel.BUFFERED)
    val eventFlow = _eventFlow.receiveAsFlow()

    private val _viewState = MutableStateFlow<TaskListViewState>(TaskListViewState.Loading)
    val viewState: StateFlow<TaskListViewState> = _viewState

    init {
        setUpEventPipelines()
        fetchTasks()
    }

    fun onViewEvent(viewEvent: TaskListViewEvent) {
        when (viewEvent) {
            TaskListViewEvent.OnCreateNewTaskClicked -> {
                viewModelScope.launch {
                    _eventFlow.send(TaskListSingleViewState.LaunchCreateTaskScreen)
                }
            }

            TaskListViewEvent.RefreshList -> fetchTasks()
        }
    }

    private fun setUpEventPipelines() {
        viewModelScope.launch {
            eventPipelines
                .tasksActionPipeline
                .filterIsInstance<TasksAction.TaskCreatedAction>()
                .collect { fetchTasks() }
        }
    }

    private fun fetchTasks() {
        viewModelScope.launch {
            kotlin.runCatching {
                taskRepository.getAllTasks()
            }.onSuccess {
                _viewState.emit(TaskListViewState.Success(it))
            }.onFailure { error ->
                logger.log(error)
                _viewState.emit(TaskListViewState.Error(error))
            }
        }
    }
}
