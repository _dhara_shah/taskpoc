package com.android.dhara.taskpoc.di

import com.android.dhara.taskpoc.repository.TaskRepository
import com.android.dhara.taskpoc.repository.mappers.TaskMapper
import com.android.dhara.taskpoc.repository.pipelines.EventPipelines
import com.android.dhara.taskpoc.repository.room.DatabaseHelper
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoryModule = module {
    single<EventPipelines> { EventPipelines() }

    single<DatabaseHelper> { DatabaseHelper(context = androidContext()) }

    factory<TaskMapper> { TaskMapper() }

    factory<TaskRepository> {
        TaskRepository(
            taskDao = get<DatabaseHelper>().database.taskDao(),
            taskMapper = get(),
            logger = get()
        )
    }
}
