package com.android.dhara.taskpoc.di

import com.android.dhara.taskpoc.logger.Logger
import com.android.dhara.taskpoc.logger.TaskPocLogger
import org.koin.dsl.module

val loggerModule = module {
    single<Logger> { TaskPocLogger() }
}
