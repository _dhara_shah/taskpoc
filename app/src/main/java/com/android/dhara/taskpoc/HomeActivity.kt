package com.android.dhara.taskpoc

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.dhara.taskpoc.databinding.ActivityHomeBinding
import com.android.dhara.taskpoc.viewbinding.viewBinding

class HomeActivity : AppCompatActivity() {
    private val binding by viewBinding(ActivityHomeBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }
}
