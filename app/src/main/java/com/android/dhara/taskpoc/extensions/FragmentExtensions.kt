package com.android.dhara.taskpoc.extensions

import android.view.View
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar

fun Fragment.showSnack(
    anchorView: View,
    @StringRes messageId: Int,
    @BaseTransientBottomBar.Duration length: Int = BaseTransientBottomBar.LENGTH_LONG,
    action: SnackBarHost.SnackAction? = null
) =
    showSnack(anchorView, getString(messageId), length, action)

fun Fragment.showSnack(
    anchorView: View,
    message: String,
    @BaseTransientBottomBar.Duration length: Int = BaseTransientBottomBar.LENGTH_LONG,
    action: SnackBarHost.SnackAction? = null
) {
    (activity as? SnackBarHost)?.showSnackBar(message, length, action) ?: if (action == null) Snackbar.make(
        anchorView,
        message,
        length
    ).show()
    else Snackbar.make(anchorView, message, length).setAction(action.actionButtonRes) { action.action() }.show()
}
