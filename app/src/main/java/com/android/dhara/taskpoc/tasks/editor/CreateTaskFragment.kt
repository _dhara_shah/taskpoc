package com.android.dhara.taskpoc.tasks.editor

import android.os.Bundle
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.android.dhara.taskpoc.R
import com.android.dhara.taskpoc.databinding.FragmentCreateTaskBinding
import com.android.dhara.taskpoc.extensions.collectInFragment
import com.android.dhara.taskpoc.extensions.showSnack
import com.android.dhara.taskpoc.tasks.editor.data.CreateTaskDialogViewState
import com.android.dhara.taskpoc.tasks.editor.data.CreateTaskSingleViewState
import com.android.dhara.taskpoc.tasks.editor.data.CreateTaskViewEvent
import com.android.dhara.taskpoc.tasks.editor.data.TextChangedViewEvent
import com.android.dhara.taskpoc.viewbinding.viewBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.koin.androidx.viewmodel.ext.android.stateViewModel

class CreateTaskFragment : Fragment(R.layout.fragment_create_task) {
    private val binding by viewBinding(FragmentCreateTaskBinding::bind)
    private val viewModel: CreateTaskViewModel by stateViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.headerToolbar.apply {
            setupWithNavController(
                findNavController(),
                AppBarConfiguration(findNavController().graph)
            )

            setNavigationOnClickListener {
                viewModel.onViewEvent(CreateTaskViewEvent.BackButtonPressed)
            }
        }

        binding.saveTaskButton.setOnClickListener {
            viewModel.onViewEvent(CreateTaskViewEvent.SaveButtonClicked)
        }

        viewModel.dialogViewState.observe(viewLifecycleOwner, ::handleDialogViewState)

        viewModel.eventFlow.collectInFragment(this, ::handleSingleViewState)

        addTextChangeListeners()
    }

    private fun addTextChangeListeners() {
        binding.taskTitleEditText.doAfterTextChanged {
            viewModel.onViewEvent(TextChangedViewEvent.TaskTitleDataChanged(it.toString()))
        }

        binding.taskDateEditText.doAfterTextChanged {
            viewModel.onViewEvent(TextChangedViewEvent.TaskDateTimeDataChanged(it.toString()))
        }

        binding.taskDescEditText.doAfterTextChanged {
            viewModel.onViewEvent(TextChangedViewEvent.TaskDescDataChanged(it.toString()))
        }
    }

    private fun handleSingleViewState(viewState: CreateTaskSingleViewState) {
        when (viewState) {
            CreateTaskSingleViewState.CloseScreen -> closeScreen()

            CreateTaskSingleViewState.ErrorSavingTask ->
                showSnack(requireView(), R.string.error_saving_task)

            CreateTaskSingleViewState.Success -> closeScreen()
        }
    }

    private fun handleDialogViewState(viewState: CreateTaskDialogViewState) {
        when (viewState) {
            CreateTaskDialogViewState.ShowConfirmCancelDialog -> {
                MaterialAlertDialogBuilder(requireContext())
                    .setMessage(R.string.are_you_sure_you_want_to_exit)
                    .setPositiveButton(android.R.string.ok) { _, _ ->
                        viewModel.onViewEvent(CreateTaskViewEvent.ConfirmCancelClicked)
                    }
                    .setNegativeButton(android.R.string.cancel) { _, _ -> }
                    .show()
            }
            CreateTaskDialogViewState.ShowSuccess -> {
                showSnack(requireView(), R.string.success_saving_task)
                closeScreen()
            }
        }
    }

    private fun closeScreen() = findNavController().navigateUp()
}
