package com.android.dhara.taskpoc

import android.app.Application
import com.android.dhara.taskpoc.di.loggerModule
import com.android.dhara.taskpoc.di.repositoryModule
import com.android.dhara.taskpoc.di.tasksModule
import net.danlew.android.joda.JodaTimeAndroid
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.module.Module

open class TaskPocApp : Application() {

    override fun onCreate() {
        super.onCreate()
        JodaTimeAndroid.init(this)
        setUpKoin()
    }

    private fun setUpKoin() {
        startKoin {
            if (BuildConfig.DEBUG) {
                androidLogger()
            }
            androidContext(applicationContext)
            modules(createKoinModulesList())
        }
    }

    private fun createKoinModulesList(): List<Module> {
        return listOf(
            loggerModule,
            tasksModule,
            repositoryModule
        )
    }
}
